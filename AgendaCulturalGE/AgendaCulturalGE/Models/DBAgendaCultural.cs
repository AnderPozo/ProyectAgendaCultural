﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace AgendaCulturalGE.Models
{
    public class DBAgendaCultural: DbContext
    {
        //Constructor
        public DBAgendaCultural()
            : base("DBAgendaCultural")
        {
        }
        public DbSet<Provincia> ProvinciaDb { get; set; }
        public DbSet<Ciudad> CiudadDb { get; set; }
        public DbSet<Direccion> DireccionDb { get; set; }
        public DbSet<Categoria> CategoriaDb { get; set; }
        public DbSet<Lugar> LugarDb { get; set; }
        public DbSet<Evento> EventoDb { get; set; }
    }
}